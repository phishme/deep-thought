Deep Thought
============

Deep Thought is a Ruby on Rails application designed to solve the Ultimate Question of Life, the Universe, and Everything.

Assignment:
-----------

Development has given you this application which has become a vital tool to our rapidly expanding customer base. As a member of the Production Engineering team your mission is to automate the application deployment using Ansible and Docker.  The Product Owner wishes this application to later become HA, so this should be deployed in Docker Swarm mode.

Details:
--------

* Ansible should be used to configure the host system and...
  * Install and configure Docker CE
  * Deploy the Docker Stack
  * Install and configure any required dependencies
* Docker should be used for...
  * Web front end
  * Rails app
  * A logging service which captures logs from the host, Docker, and the applications and saves them to peristent storage

Format:
-------

Do not post your solution to public Git repositories. Instead please work locally:
  1. `mkdir homework && cd homework`
  2. `git init`
  3. `touch README.md`
  4. `git commit -m "init commit"`
  5. < ...hack, hack, hack... >
  6. `cd .. &&  git clone --mirror homework homework.repo`
  7. `tar -czf homework.repo.tgz homework.repo`

Your commits should clearly communicate your intent and design choices, if any, made while completing the task

We would also prefer you commit all your work including mistakes so we can see how you worked through problems you encounter 


Review:
-------

-   We will clone your repository to an EC2 instance to deploy the application,
    troubleshoot, and discuss.
-   Additionally you might want to be prepared to discuss the following topics
    -   Ways this application could be made reasonably resilient (i.e. a single
        node failure does not affect end users).
    -   Ways this application could be scaled, automatically if possible, to
        handle increased loads.


[Additional
Research](http://en.wikipedia.org/wiki/Phrases\_from\_The\_Hitchhiker%27s\_Guide\_to\_the\_Galaxy\#Answer\_to\_the\_Ultimate\_Question\_of\_Life.2C\_the\_Universe.2C\_and\_Everything\_.2842.29)
